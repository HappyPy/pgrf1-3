package view;

import domain.*;
import domain.cubic.Curve;
import domain.axis.AxisX;
import domain.axis.AxisY;
import domain.axis.AxisZ;
import domain.cubic.CubicGrid3D;
import rasterizer.RasterizerImpl;
import render.Renderer;
import transform.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;

public class View extends JFrame {

    @FunctionalInterface
    interface KeyAction {
        void doAction();
    }

    private Mat4 orthogoval = new Mat4OrthoRH(5, 5, 0.1, 15);
    private Mat4 prespective = new Mat4Identity();
    private static final float cameraSpeed = 1f;
    private static final float scaleUp = 1.1f;
    private static final float scaleDown = 0.9f;
    private HashMap<Integer, KeyAction> keyDispatcher = new HashMap<>();
    private JPanel panel;
    private BufferedImage img;
    private Renderer renderer;
    private Camera camera = new Camera()
            .withPosition(new Vec3D(8, 3, 3))
            .withAzimuth(3.3)
            .withZenith(-0.35);
    private Solid circle = new Circle(new Point3D(2.7, 2.7, 0), 1.8f, 50);
    private Solid cube = new Cube();
    private Solid grid = new Grid(6, 6);
    private Mat4 solidTransform = new Mat4Identity();
    private Solid axisX = new AxisX();
    private Solid axisY = new AxisY();
    private Solid axisZ = new AxisZ();
    private Solid bSolid = new CubicGrid3D(20, 2, Cubic.BEZIER);
    private Solid fSolid = new CubicGrid3D(20, 2, Cubic.FERGUSON);
    private Solid cSolid = new CubicGrid3D(20, 2, Cubic.COONS);
    private Solid bCubic = new Curve(Cubic.BEZIER);
    private Solid fCubic = new Curve(Cubic.FERGUSON);
    private Solid cCubic = new Curve(Cubic.COONS);
    private ArrayList<Solid> staticSolids = new ArrayList<>();

    public View(int width, int height) {
        img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        setLayout(new BorderLayout());
        setTitle("UHK FIM PGRF : " + this.getClass().getName());
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        panel = new JPanel() {
            private static final long serialVersionUID = 1L;

            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                present(g);
            }
        };
        panel.setPreferredSize(new Dimension(width, height));

        add(panel);
        pack();
        setVisible(true);

        keyDispatcher.put(KeyEvent.VK_W, () -> camera = camera.forward(cameraSpeed));
        keyDispatcher.put(KeyEvent.VK_S, () -> camera = camera.backward(cameraSpeed));
        keyDispatcher.put(KeyEvent.VK_A, () -> camera = camera.left(cameraSpeed));
        keyDispatcher.put(KeyEvent.VK_D, () -> camera = camera.right(cameraSpeed));
        keyDispatcher.put(KeyEvent.VK_O, () -> renderer.setProjection(orthogoval));
        keyDispatcher.put(KeyEvent.VK_P, () -> renderer.setProjection(prespective));
        keyDispatcher.put(KeyEvent.VK_U, () -> solidTransform = solidTransform.mul(new Mat4Scale(scaleUp)));
        keyDispatcher.put(KeyEvent.VK_I, () -> solidTransform = solidTransform.mul(new Mat4Scale(scaleDown)));


        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                try {
                    keyDispatcher.get(e.getKeyCode()).doAction();
                } catch (NullPointerException exception) {
                    System.out.println("That button doesn't do anything yet... " + e.getKeyCode());
                } finally {
                    draw();
                }
            }
        });
        requestFocus();
        MouseAdapter mouseAdapter = new MouseAdapter() {
            int x, y;

            @Override
            public void mousePressed(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    x = e.getX();
                    y = e.getY();
                }
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    int x1 = e.getX();
                    int y1 = e.getY();
                    float newA = (x1 - x) / 100f;
                    float newZ = (y1 - y) / 100f;
                    camera = camera.addAzimuth(newA);
                    camera = camera.addZenith(newZ);
                    x = x1;
                    y = y1;
                    draw();
                }
            }

        };
        addMouseListener(mouseAdapter);
        addMouseMotionListener(mouseAdapter);
    }

    public void start() {
        renderer = new Renderer(new RasterizerImpl(img));
        solidTransform = new Mat4RotX(Math.PI / 4).mul(new Mat4RotY(-Math.PI / 4)).mul(new Mat4Transl(2, 2, 0));
        prespective = new Mat4PerspRH(0.7853981633974483D, (double) ((float) this.img.getHeight() / (float) this.img.getWidth()), 0.01D, 100.0D);
        renderer.setProjection(prespective);
        addStaticSolid();
        draw();
        panel.repaint();
    }

    public void addStaticSolid() {
        staticSolids.add(grid);
        grid.setTrans(new Mat4Scale(4).mul(new Mat4Transl(1, 1, 0)));
        staticSolids.add(axisX);
        staticSolids.add(axisY);
        staticSolids.add(axisZ);
        staticSolids.add(circle);
        staticSolids.add(bCubic);
        bCubic.setTrans(new Mat4Transl(-1, 0, 0));
        staticSolids.add(fCubic);
        fCubic.setTrans(new Mat4Transl(-1, 5, 0));
        staticSolids.add(cCubic);
        cCubic.setTrans(new Mat4Transl(-1, -5, 0));
        staticSolids.add(bSolid);
        bSolid.setTrans(new Mat4Transl(-5, 0, 0));
        staticSolids.add(fSolid);
        fSolid.setTrans(new Mat4Transl(-5, 5, 0));
        staticSolids.add(cSolid);
        cSolid.setTrans(new Mat4Transl(-5, -5, 0));
    }

    public void present(Graphics graphics) {
        graphics.drawImage(img, 0, 0, null);
    }

    private void draw() {
        clear();
        renderer.setView(camera.getViewMatrix());
        cube.setTrans(solidTransform);
        renderer.draw(cube);
        renderer.draw(staticSolids);
        panel.repaint();
    }

    private void clear() {
        img.getGraphics().fillRect(0, 0, img.getWidth(), img.getHeight());
    }
}
