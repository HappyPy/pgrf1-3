package domain;

import transform.Point3D;

import java.util.ArrayList;
import java.util.List;

public class Grid extends Solid {

    public Grid(int n, int m) {
        int m1 = ++m;
        int n1 = ++n;
        List<Point3D> vertexBuffer = new ArrayList<>();
        List<Integer> indexBuffer = new ArrayList<>();
        setColor(0x000000);
        for (int i = 0; i < n1; i++) {
            for (int j = 0; j < m1; j++) {
                vertexBuffer.add(new Point3D(i * 1f / n1, j * 1f / m1, 0));
            }
        }

        for (int i = 0; i < n1; i++) {
                indexBuffer.add(i);
                indexBuffer.add(i + (n1 * (m1 - 1)));
        }
        for (int i = 0; i < m1; i++) {
            indexBuffer.add(i * m1);
            indexBuffer.add(i * m1 + n1 - 1);
        }
        setVertexBuffer(vertexBuffer);
        setIndexBuffer(indexBuffer);
    }
}
