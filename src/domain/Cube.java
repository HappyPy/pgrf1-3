package domain;

import transform.Mat4Identity;
import transform.Point3D;

import java.util.Arrays;

public class Cube extends Solid {

    public Cube() {
        Point3D[] point3DS = {
                new Point3D(0, 0, 0),
                new Point3D(0, 0, 1),
                new Point3D(1, 0, 0),
                new Point3D(0, 1, 0),
                new Point3D(1, 1, 0),
                new Point3D(1, 0, 1),
                new Point3D(0, 1, 1),
                new Point3D(1, 1, 1),
        };
        setVertexBuffer(Arrays.asList(point3DS));
        Integer[] indexes = {
                0, 1,
                0, 3,
                0, 2,
                3, 6,
                3, 4,
                2, 4,
                2, 5,
                1, 6,
                1, 5,
                5, 7,
                6, 7,
                4, 7,
        };
        setIndexBuffer(Arrays.asList(indexes));
        setTrans(new Mat4Identity());
        setColor(0x00FF00);
    }
}
