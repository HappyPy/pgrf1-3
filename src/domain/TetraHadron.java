package domain;

import transform.Mat4Identity;
import transform.Point3D;

import java.util.Arrays;

public class TetraHadron extends Solid {

    public TetraHadron() {
        Point3D[] point3DS = {
                new Point3D(0, 0, 0),
                new Point3D(0, 0, 1),
                new Point3D(1, 0, 0),
                new Point3D(0, 1, 0),
        };
        setVertexBuffer(Arrays.asList(point3DS));
        Integer[] indexes = {
                0, 1,
                0, 3,
                0, 2,
                1, 3,
                3, 2,
                1, 2,

        };
        setIndexBuffer(Arrays.asList(indexes));
        setTrans(new Mat4Identity());
        setColor(0x0000FF);
    }
}
