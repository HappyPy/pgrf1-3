package domain;

import transform.Mat4;
import transform.Mat4Identity;
import transform.Point3D;

import java.util.List;

public abstract class Solid {
    private List<Point3D> vertexBuffer;
    private List<Integer> indexBuffer;
    private Mat4 trans = new Mat4Identity();
    private int color = 0x000000;

    public List<Point3D> getVertexBuffer() {
        return vertexBuffer;
    }

    public void setVertexBuffer(List<Point3D> vertexBuffer) {
        this.vertexBuffer = vertexBuffer;
    }

    public List<Integer> getIndexBuffer() {
        return indexBuffer;
    }

    public void setIndexBuffer(List<Integer> indexBuffer) {
        this.indexBuffer = indexBuffer;
    }

    public Mat4 getTrans() {
        return trans;
    }

    public void setTrans(Mat4 trans) {
        this.trans = trans;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

}
