package domain.cubic;

import domain.Solid;
import transform.Cubic;
import transform.Mat4;
import transform.Point3D;

import java.util.ArrayList;
import java.util.List;

public class CubicGrid3D extends Solid {
    private List<Point3D> vertexBuffer = new ArrayList<>();
    private List<Integer> indexBuffer = new ArrayList<>();
    private double r;
    private int n;
    private Mat4 cubicMatrix;

    public CubicGrid3D(int n, double r, Mat4 cubicMatrix) {
        this.n = Math.max(n, 3);
        this.r = r;
        this.cubicMatrix = cubicMatrix;
        createGrid();
        setColor(0x000000);
        setIndexBuffer(indexBuffer);
        setVertexBuffer(vertexBuffer);
    }

    public void createGrid() {
        for (int i = 0; i < this.n; i++) {
            createLine(i);
        }
    }

    public void createLine(int i) {
        double cosY = r * Math.cos(i * 2 * Math.PI / this.n);
        double cosX = r * Math.sin(i * 2 * Math.PI / this.n);

        Point3D[] vertexes = {
                new Point3D(0 + cosX, cosY, 0),
                new Point3D(0 + cosX, cosY, 2),
                new Point3D(2 + cosX, cosY, 0.5),
                new Point3D(2 + cosX, cosY, 2),
        };

        List<Point3D> vertexBuffer = new ArrayList<>();
        List<Integer> indexBuffer = new ArrayList<>();
        Cubic3D.create(vertexes, cubicMatrix, vertexBuffer, indexBuffer, 0.01d);
        this.vertexBuffer.addAll(vertexBuffer);

        // draw vertical lines
        for (Integer integer : indexBuffer) {
            this.indexBuffer.add(integer + (i * vertexBuffer.size()));
        }

        // draw horizontal lines
        for (int j = 0; j < indexBuffer.size(); j++) {
            if (j % n == 0 || j == indexBuffer.size() - 1) {
                if (i != 0) {
                    this.indexBuffer.add(indexBuffer.get(j) + (i * vertexBuffer.size()));
                    this.indexBuffer.add(indexBuffer.get(j) + ((i - 1) * vertexBuffer.size()));
                } else {
                    this.indexBuffer.add(indexBuffer.get(j) + (i * vertexBuffer.size()));
                    this.indexBuffer.add(indexBuffer.get(j) + ((this.n - 1) * vertexBuffer.size()));
                }

            }
        }
    }
}
