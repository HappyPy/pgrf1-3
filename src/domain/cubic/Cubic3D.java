package domain.cubic;

import transform.Cubic;
import transform.Mat4;
import transform.Point3D;

import java.util.List;

public  class Cubic3D {

    public static void create(Point3D[] vertexes, Mat4 matrix, List<Point3D> vertexBuffer, List<Integer> indexes, double step) {
        Cubic cubic = new Cubic(matrix, vertexes[0], vertexes[1], vertexes[2], vertexes[3]);
        int i = 0;
            vertexBuffer.add(cubic.compute(0));
            for (double a = step; a <= 1; a += step) {
                Point3D p1 = cubic.compute(a);
                vertexBuffer.add(p1);
                indexes.add(i);
                if (i < (1/step) - 1){
                    indexes.add(++i);
                }
        }
    }
}
