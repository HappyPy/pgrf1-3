package domain.cubic;

import domain.Solid;
import transform.Cubic;
import transform.Mat4;
import transform.Point3D;

import java.util.ArrayList;
import java.util.List;

public class Curve extends Solid {

    public Curve(Mat4 matrix) {
        Point3D[] vertexes = {
                new Point3D(0 , 0, 0),
                new Point3D(0 , 0, 1.5),
                new Point3D(2 , 0, 0.5),
                new Point3D(2 , 0, 2),
        };
        setColor(0xFF00FF);
        List<Point3D> vertexBuffer = new ArrayList<>();
        List<Integer> indexBuffer = new ArrayList<>();
        Cubic3D.create(vertexes, matrix, vertexBuffer, indexBuffer, 0.01d);
        setVertexBuffer(vertexBuffer);
        setIndexBuffer(indexBuffer);
    }
}
