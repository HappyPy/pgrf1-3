package domain;

import transform.Mat4Identity;
import transform.Point3D;

import java.util.ArrayList;
import java.util.List;

public class Circle extends Solid {

    public Circle(Point3D center, float r, int n){
        List<Point3D> vertexBuffer = new ArrayList<>();
        List<Integer> indexBuffer = new ArrayList<>();
        setColor(0xFF0000);
        if (n < 3){
            n = 3;
        }
        for(int i = 0; i < n; i++){
            indexBuffer.add(i);
            double x  =  center.getX() + r * Math.cos(i * 2 * Math.PI / n);
            double y  =  center.getY() + r * Math.sin(i * 2 * Math.PI / n);
            vertexBuffer.add(new Point3D(x, y, 0));
            if (i + 1 < n){
                indexBuffer.add(i + 1);
            }
        }
        indexBuffer.add(0);
        setVertexBuffer(vertexBuffer);
        setIndexBuffer(indexBuffer);
        setTrans(new Mat4Identity());
    }
}
