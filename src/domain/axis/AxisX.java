package domain.axis;

import domain.Solid;
import transform.Point3D;
import java.util.Arrays;

public class AxisX extends Solid {

    public AxisX() {
        Point3D[] vertexes = {
                new Point3D(0, 0, 0),
                new Point3D(1, 0, 0),
                new Point3D(0.8, 0.1, 0),
                new Point3D(0.8, -0.1, 0),
                new Point3D(0.8, 0, 0.1),
                new Point3D(0.8, 0, -0.1),
        };
        Integer[] indexes = {
                0, 1,
                1, 2,
                1, 3,
                1, 4,
                1, 5,
                5, 3,
                2, 4,
                4, 3,
                5, 2,
        };
        setColor(0xFF0000);
        setVertexBuffer(Arrays.asList(vertexes));
        setIndexBuffer(Arrays.asList(indexes));
    }
}
