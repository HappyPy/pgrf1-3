package domain.axis;

import domain.Solid;
import transform.Point3D;

import java.util.Arrays;

public class AxisZ extends Solid {

    public AxisZ() {
        Point3D[] vertexes = {
                new Point3D(0, 0, 0),
                new Point3D(0, 0, 1),
                new Point3D(0.1, 0, 0.8),
                new Point3D(-0.1, 0, 0.8),
                new Point3D(0, 0.1, 0.8),
                new Point3D(0, -0.1, 0.8),
        };
        Integer[] indexes = {
                0, 1,
                1, 2,
                1, 3,
                1, 4,
                1, 5,
                5, 3,
                2, 4,
                4, 3,
                5, 2,
        };
        setVertexBuffer(Arrays.asList(vertexes));
        setIndexBuffer(Arrays.asList(indexes));
        setColor(0x0000FF);
    }
}
