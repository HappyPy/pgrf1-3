package rasterizer;

import transform.Vec2D;
import transform.Vec3D;

import java.awt.*;
import java.awt.image.BufferedImage;

public class RasterizerImpl implements Rasterizer {
    BufferedImage img;

    public RasterizerImpl(BufferedImage img) {
        this.img = img;
    }

    @Override
    public void drawLine(Vec2D v1, Vec2D v2, int color) {
        Graphics graphics = img.getGraphics();
        graphics.setColor(Color.RED);
        Vec2D start = doViewportTransform(v1);
        Vec2D end = doViewportTransform(v2);
        graphics.setColor(new Color(color));
        graphics.drawLine((int) start.getX(), (int) start.getY(), (int) end.getX(), (int) end.getY());
    }


    private Vec2D doViewportTransform(Vec2D vec) {
        double y = (img.getWidth() - 1) * (1 - vec.getY()) / 2;
        double x = (img.getHeight() - 1) * (1 + vec.getX()) / 2;
        return new Vec2D(x, y);
    }
}
