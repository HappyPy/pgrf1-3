package rasterizer;

import transform.Vec2D;

public interface Rasterizer {
    public void drawLine(Vec2D v1, Vec2D v2, int color);
}
