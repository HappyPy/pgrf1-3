package render;

import domain.Solid;
import rasterizer.Rasterizer;
import transform.Mat4;
import transform.Mat4Identity;
import transform.Point3D;
import transform.Vec3D;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Renderer {

    private Rasterizer rasterizer;
    private Mat4 view;
    private Mat4 projection;

    public Renderer(Rasterizer rasterizer) {
        this.rasterizer = rasterizer;
        this.view = new Mat4Identity();
        this.projection = new Mat4Identity();
    }

    public void draw(List<Solid> solids){
        solids.forEach(this::draw);
    }

    public void draw(Solid solid) {
        List<Point3D> vertexes = new ArrayList<>();
        Mat4 transform = solid.getTrans().mul(view).mul(projection);
        for (Point3D point : solid.getVertexBuffer()) {
            vertexes.add(point.mul(transform));
        }
        List<Integer> indexBuffer = solid.getIndexBuffer();
        for (int i = 0; i <= indexBuffer.size() - 1; i = i + 2) {
            Point3D a = vertexes.get(indexBuffer.get(i));
            Point3D b = vertexes.get(indexBuffer.get(i + 1));
            renderLine(a, b, solid.getColor());
        }
    }

    public void renderLine(Point3D a, Point3D b, int color) {
        if (cut(a)) {
            if (cut(b)) {
                Optional<Vec3D> deA = a.dehomog();
                Optional<Vec3D> deB = b.dehomog();
                if (deA.isPresent() && deB.isPresent()) {
                    Vec3D vecA = deA.get();
                    Vec3D vecB = deB.get();
                    rasterizer.drawLine(vecA.ignoreZ(), vecB.ignoreZ(), color);
                }
            }
        }
    }

    public boolean cut(Point3D point) {
        if (-point.getW() <= point.getX() && point.getY() <= point.getW()) {
            return point.getZ() >= 0 && point.getZ() <= point.getW();
        }
        return false;
    }

    public void setView(Mat4 view) {
        this.view = view;
    }

    public void setProjection(Mat4 projection) {
        this.projection = projection;
    }
}
